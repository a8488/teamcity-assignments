import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.python
import jetbrains.buildServer.configs.kotlin.v2019_2.sharedResource
import jetbrains.buildServer.configs.kotlin.v2019_2.triggers.vcs
import jetbrains.buildServer.configs.kotlin.v2019_2.vcs.GitVcsRoot

/*
The settings script is an entry point for defining a TeamCity
project hierarchy. The script should contain a single call to the
project() function with a Project instance or an init function as
an argument.

VcsRoots, BuildTypes, Templates, and subprojects can be
registered inside the project using the vcsRoot(), buildType(),
template(), and subProject() methods respectively.

To debug settings scripts in command-line, run the

    mvnDebug org.jetbrains.teamcity:teamcity-configs-maven-plugin:generate

command and attach your debugger to the port 8000.

To debug in IntelliJ Idea, open the 'Maven Projects' tool window (View
-> Tool Windows -> Maven Projects), find the generate task node
(Plugins -> teamcity-configs -> teamcity-configs:generate), the
'Debug' option is available in the context menu for the task.
*/

version = "2021.2"

project {

    vcsRoot(ToolConnector)
    vcsRoot(SharedRepo)

    buildType(Build)

    features {
        sharedResource {
            id = "PROJECT_EXT_4"
            name = "ToolConnector"
            resourceType = infinite()
        }
    }
}

object Build : BuildType({
    name = "Build"

    vcs {
        root(DslContext.settingsRoot)
        root(SharedRepo, "+:. => ./SharedRepo")
        root(ToolConnector, "+:. => ./ToolConnector")
    }

    steps {
        python {
            command = file {
                filename = "file.py"
            }
        }
        python {
            command = file {
                filename = "testfile.py"
            }
        }
    }

    triggers {
        vcs {
        }
    }
})

object SharedRepo : GitVcsRoot({
    name = "SharedRepo"
    url = "https://github.com/NIKHILJIND/teamcity-assignments.git"
    branch = "refs/heads/main"
    branchSpec = "refs/heads/*"
    authMethod = password {
        userName = "NIKHILJIND"
        password = "credentialsJSON:4ad57665-af22-4ea2-af5d-3bb8e1e8858d"
    }
})

object ToolConnector : GitVcsRoot({
    name = "ToolConnector"
    url = "https://gitlab.com/abc583/Resources.git"
    branch = "refs/heads/main"
    branchSpec = "refs/heads/*"
    authMethod = password {
        userName = "NIKHILJINDAL"
        password = "credentialsJSON:502df4cf-135a-4728-af6c-c8ca074b4f02"
    }
})
